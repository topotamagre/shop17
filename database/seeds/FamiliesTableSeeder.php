<?php

use Illuminate\Database\Seeder;

class FamiliesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('families')->insert([
            'code' => 'MICR',
            'name' =>  'Microprocesadores',
            ]);
        DB::table('families')->insert([
            'code' => 'TV',
            'name' =>  'Televisores',
            ]);
        DB::table('families')->insert([
            'code' => 'INF',
            'name' =>  'Material Informatico',
            ]);

    }
}
