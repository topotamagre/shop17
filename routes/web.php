<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::resource('/family', 'FamilyController');

Route::get('/family/', function(){
    return view('family');
});

Route::get('family', 'FamilyController@index');
Route::get('family/create', 'FamilyController@create');
Route::post('family/create', 'FamilyController@store');
Route::post('family/update', 'FamilyController@save');
Route::get('family/update/{id}', 'FamilyController@update');
Route::get('family/delete/{id}', 'FamilyController@delete');