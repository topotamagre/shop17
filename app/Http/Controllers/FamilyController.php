<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\FamilyRequest;
use App\Family;

class FamilyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $families = Family::all();
            return view('family.index', ['families' => $families]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('family.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
     // dd($request->all());
        $this->validate($request, [
            'code' => 'required|unique:families',
            'name' => 'required',

            ]);

            // 'name' => 'required|unique:posts|max:255',

        $family = new Family($request->all());
        // dd($study);
        $family->save();

        return redirect('/family');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    public function show($id, Request $request)
    {
        
        $families = Family::find(2); //existe seguro
        //$families = Family::find0rFail(2); si no sabes si existe
            if($request->ajax()){
                return $family;
            }else{
                return view ('family.show');
            }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function save(FamilyRequest $request)
    {
        // $id = $request['id'];
        // dd($request);
        $family = Family::find($request->id);
        $family->code = $request->code;
        $family->name = $request->name;
        $family->save();
        return redirect('/family');
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
            {
        $family = Family::find($id);
        return view('family.update', ['family' => $family]);
        // dd($family);
    }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        // modo 1
        // $study = Study::find($id);
        // $study->delete();
        // modo 2
        Family::destroy($id);

        return redirect('/family');

    }
}
