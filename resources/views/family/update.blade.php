@extends('layouts.app')

@section('title', 'Tienda con Laravel')

@section('content')
    <h1>Modificacion de categoria</h1>

    <form method="post" action="/family/update">
         {{ csrf_field() }}
         <input type="hidden" name="id" value="{{ $family['id'] }}"><br>
         <div>
         <label>Código</label>
         <input type="text" name="code" value="{{ $family['code'] }}">
        {{ $errors->first('code') }}
        </div>

        <div>
         <label>Nombre</label>
         <input type="text" name="name" value="{{ $family['name'] }}">
         {{ $errors->first('name') }}
        </div>



         
         <label></label>
         <input type="submit" value="Enviar"><br>
    </form>

@stop