@extends('layouts.app')

@section('title', 'Tienda con Laravel')

@section('content')
    <h1>Lista de categorias</h1>

    <p><a href="/family/create">Nuevo</a></p>
    <table class="table">
        <tr>
            <th>Id</th>
            <th>Cod.</th>
            <th>Nombre</th>
            <th></th>
        </tr>

        @foreach($families as $family)
        <tr>
        <!-- observa que podemos recuperar atributos de dos modos: -->
            <td>{{ $family['id'] }}</td>
            <td>{{ $family['code'] }}</td>
            <td>{{ $family['name'] }}</td>

            <td><a href="/family/update/{{ $family['id'] }}">Editar</a>
                <a href="/family/delete/{{ $family->id }}">Borrar</a>
            </td>
        </tr>
        @endforeach
        </table>

    <p><a href="/family/create">Nuevo</a></p>

@stop