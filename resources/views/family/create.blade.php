@extends('layouts.app')

@section('title', 'Tienda con Laravel')

@section('content')
    <h1>Alta de categorias</h1>

    <form method="post" action="/family/create">
         {{ csrf_field() }}
         <div>
         <label>Código</label>
         <input type="text" name="code" value="{{ old('code') }}">
        {{ $errors->first('code') }}
        </div>

        <div>
         <label>Nombre</label>
         <input type="text" name="name" value="{{ old('name') }}">
         {{ $errors->first('name') }}
         </div>

        <label></label>
        <input type="submit" value="Enviar"><br>
    </form>

    <ul>
    @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
    @endforeach
    </ul>
@stop